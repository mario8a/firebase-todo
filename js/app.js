//HTML elements
const todoInputTitle = document.querySelector('#todo-title');
const todoInputDesc = document.querySelector('#todo-desc');
const todoForm = document.getElementById('todo-form');

const todosContainer = document.getElementById('todos-container');

let todos = [];

todoForm.addEventListener('submit', async (e) => {
  e.preventDefault()
  
  const title = todoInputTitle.value;
  const description = todoInputDesc.value;
  
  if (title !== '' && description !== '') {
    createTodo(title, description);
  }
  todoForm.reset();
});

window.addEventListener('DOMContentLoaded', (e) => {
  onGetTasks()
});



const loadTodos = async () => {
  todosContainer.innerHTML = "";
  todos = [];
  try {
    const response = await getTodos();
    todos = [...response];
    // console.log('loadTodos',todos)
    renderTodos();
  } catch (error) {
    
  }
}

const renderTodos = () => {
  let html = "";
  todos.forEach((todo) => {
    // console.log('renderTodos', todo)
    html += `
    <div class="col">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">${todo.title}</h5>
          <p class="card-text">${todo.desc}</p>
        </div>
        <button class="btn btn-danger" id="delete-btn">Eliminar</button>
      </div>
    </div>
    `;
  });
  todosContainer.innerHTML = html;
}

/* =============================
 * FIREBASE
 * =============================
 **/

// Save firestore
//https://firebase.google.com/docs/firestore/manage-data/add-data#web-version-8
const createTodo = async (title, desc) => {
  try {
    const response = await db.collection('todos').add({
      title,
      desc
    });
    return response;
  } catch (error) {
    throw new Error(error);
  }
};

//https://firebase.google.com/docs/firestore/query-data/queries#web-version-8_3
const getTodos = async () => {
  try {
    let items = [];
    const response = await db.collection('todos').get();
    // console.log(response);
    response.forEach(item => {
      items.push(item.data())
      // console.log(item.id);
      // console.log(item)
    });
    return items;
  } catch (error) {
    throw new Error(error);
  }
}

//https://firebase.google.com/docs/firestore/query-data/listen
const onGetTasks = () => {
  try {
    db.collection("todos").onSnapshot(() => {
      loadTodos();
    });
  } catch (error) {
    
  }
}